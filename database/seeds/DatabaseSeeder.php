<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('kategori')->insert([
            'nama' => 'gadget'
        ]);

        DB::table('kategori')->insert([
            'nama' => 'otomotif'
        ]);

        DB::table('produk')->insert([
            'nama' => 'Handphone Murah',
            'harga' => 1500000,
            'gambar' => 'https://source.unsplash.com/600x600/?phone',
            'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis odio et turpis finibus placerat vitae non nunc. Vestibulum suscipit lacus quis diam fermentum luctus. Sed facilisis lacus sed lectus condimentum tempus. Nulla mattis, mi quis mattis tempor, sem massa volutpat tortor, in tristique diam massa non nisl. Nunc ac efficitur enim. Praesent odio justo, scelerisque ut diam quis, bibendum consequat metus. Aenean metus arcu, tempor tempor mollis non, ultricies egestas mauris. Suspendisse augue neque, accumsan at lobortis a, maximus non diam. Donec lobortis, ex eget semper tempor, tellus nisi accumsan odio, ac tempus elit arcu eu mauris. Nunc cursus diam elit, non sollicitudin dolor finibus eget. Etiam nec leo ut nisi convallis dictum non vitae dui. Curabitur orci diam, ultrices non imperdiet a, fermentum et arcu. Cras vulputate nisl quis elit finibus, et dignissim lacus laoreet.',
            'kategori_id' => 1
        ]);

        DB::table('produk')->insert([
            'nama' => 'Helm Keren',
            'harga' => 500000,
            'gambar' => 'https://source.unsplash.com/600x600/?motor',
            'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis odio et turpis finibus placerat vitae non nunc. Vestibulum suscipit lacus quis diam fermentum luctus. Sed facilisis lacus sed lectus condimentum tempus. Nulla mattis, mi quis mattis tempor, sem massa volutpat tortor, in tristique diam massa non nisl. Nunc ac efficitur enim. Praesent odio justo, scelerisque ut diam quis, bibendum consequat metus. Aenean metus arcu, tempor tempor mollis non, ultricies egestas mauris. Suspendisse augue neque, accumsan at lobortis a, maximus non diam. Donec lobortis, ex eget semper tempor, tellus nisi accumsan odio, ac tempus elit arcu eu mauris. Nunc cursus diam elit, non sollicitudin dolor finibus eget. Etiam nec leo ut nisi convallis dictum non vitae dui. Curabitur orci diam, ultrices non imperdiet a, fermentum et arcu. Cras vulputate nisl quis elit finibus, et dignissim lacus laoreet.',
            'kategori_id' => 2
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'roles' => 'admin'
        ]);
    }
}
