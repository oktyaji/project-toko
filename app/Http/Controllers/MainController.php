<?php

namespace App\Http\Controllers;

use App\ProdukModel;
use App\TransaksiModel;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function beranda() {
        return view('pages.beranda');
    }

    public function shop() {
        $produks = ProdukModel::all();
        return view('pages.shop', compact('produks'));
    }

    public function dasbor() {
        $transaksi = TransaksiModel::all();
        return view('pages.dasbor', compact('transaksi'));
    }
}
