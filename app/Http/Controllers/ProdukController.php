<?php

namespace App\Http\Controllers;

use App\KategoriModel;
use App\ProdukModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produks = ProdukModel::all();
        return view('admin.produk.index', compact('produks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategories = KategoriModel::all();
        return view('admin.produk.create', compact('kategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'kategori_id' => 'required',
            'harga' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'deskripsi' => 'required'
        ]);

        $namaGambar = time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('gambar'), $namaGambar);

        $produk = new ProdukModel;

        $produk->nama = $request->nama;
        $produk->kategori_id = $request->kategori_id;
        $produk->harga = $request->harga;
        $produk->gambar = $namaGambar;
        $produk->deskripsi = $request->deskripsi;
        $produk->save();

        return redirect('/produk')->with('success', 'Produk berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = ProdukModel::findOrFail($id);
        return view('admin.produk.show', compact('produk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = ProdukModel::findOrFail($id);
        $kategories = KategoriModel::all();
        return view('admin.produk.edit', compact('produk', 'kategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'kategori_id' => 'required',
            'harga' => 'required',
            'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'deskripsi' => 'required'
        ]);

        $produk = ProdukModel::find($id);

        if ($request->has('gambar')) {
            $namaGambar = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('gambar'), $namaGambar);

            $update = ProdukModel::where('id', $id)->update([
                'nama' => $request['nama'],
                'kategori_id' => $request['kategori_id'],
                'harga' => $request['harga'],
                'gambar' => $namaGambar,
                'deskripsi' => $request['deskripsi'],
            ]);
        } else {
            $update = ProdukModel::where('id', $id)->update([
                'nama' => $request['nama'],
                'kategori_id' => $request['kategori_id'],
                'harga' => $request['harga'],
                'deskripsi' => $request['deskripsi'],
            ]);
        }

        return redirect('/produk')->with('success', 'Produk berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = ProdukModel::find($id);

        $path = 'gambar/';
        File::delete($path . $produk->gambar);
        $produk->delete();

        return redirect('/produk')->with('success', 'Produk berhasil dihapus');
    }
}
