<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiModel extends Model
{
    protected $table = 'transaksi';
    protected $guarded = ['id'];

    public function produki() {
        return $this->belongsTo(ProdukModel::class);
    }
}
