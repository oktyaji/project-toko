<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoModel extends Model
{
    protected $table = 'foto';
    protected $guarded = ['id'];
}
