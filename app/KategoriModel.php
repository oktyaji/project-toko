<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriModel extends Model
{
    protected $table = 'kategori';
    protected $guarded = ['id'];

    public function produk() {
        return $this->hasMany(ProdukModel::class);
    }
}
