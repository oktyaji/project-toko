<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@beranda');
Route::get('/shop', 'MainController@shop');
Route::get('/dasbor', 'MainController@dasbor')->middleware('auth');

Route::middleware(['auth', 'admin'])->group(function () {
    Route::resource('kategori', 'KategoriController');
    Route::resource('produk', 'ProdukController');
    Route::resource('galeri', 'GaleriController');
    Route::resource('ulasan', 'UlasanController');
    Route::resource('foto', 'FotoController');
    Route::resource('profile', 'ProfileController');
    Route::resource('transaksi', 'TransaksiController');
});

Auth::routes();
