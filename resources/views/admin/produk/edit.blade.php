@extends('layouts.admin')

@push('script')
    <script src="https://cdn.tiny.cloud/1/r1zpl5pdiorucieme2hfrsjdbhfjt6u58u3nrok9r4emlneh/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
          toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
          toolbar_mode: 'floating',
          tinycomments_mode: 'embedded',
          tinycomments_author: 'Author name',
        });
    </script>
@endpush

@section('content')
    <a class="btn btn-info mb-4" href="/produk">Kembali</a>
    <div class="card shadow mb-4">
        <div class="card-body p-3">
                <div class="header text-dark">
                    <h2 class="font-weight-bold">Edit Produk : {{ $produk->nama }}</h2>
                    <hr />
                </div>
                <form action="/produk/{{ $produk->id }}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama Produk</label>
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Kategori" value="{{ old('nama', $produk->nama) }}">
                    </div>
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="kategori_id">Kategori</label>
                        <select class="custom-select" name="kategori_id">
                            @foreach ($kategories as $kategori)
                                @if (old('kategori_id', $produk->kategori->id) == $kategori->id)
                                    <option value="{{ $kategori->id }}" selected>{{ $kategori->nama }}</option>
                                @else
                                    <option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga Produk</label>
                        <input type="number" name="harga" class="form-control" id="harga" placeholder="Harga Produk" value="{{ old('harga', $produk->harga) }}">
                    </div>
                    @error('harga')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="gambar">File Gambar</label>
                        <input type="file" name="gambar" class="form-control" id="gambar">
                    </div>
                    @error('gambar')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi Tugas</label>
                        <textarea class="form-control" name="deskripsi" id="deskripsi" rows="3">{{ old('deskripsi', $produk->deskripsi) }}</textarea>
                    </div>
                    @error('deskripsi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
    </div>
@endsection
