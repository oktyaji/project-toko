@extends('layouts.admin')

@section('content')
    <a class="btn btn-info mb-4" href="/produk">Kembali</a>
    <div class="card shadow mb-4">
        <div class="card-body p-5">
            <h2 class="mb-3">{{ $produk->nama }}</h2>
            <hr />
            <img class="shadow-md border" width="300px" src="{{ asset('gambar/' . $produk->gambar) }}" alt="">
            <p class="my-3">Kategori Produk : <b>{{ $produk->kategori->nama }}</b></p>
            <p class="font-weight-bold">Deskripsi: </p>
            <p>{!! $produk->deskripsi !!}</p>
        </div>
    </div>
@endsection
