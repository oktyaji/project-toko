@extends('layouts.admin')

@section('content')
    <!-- Main Content -->
    <div id="content">
        @if ( session('success') )
            <div class="alert alert-success fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <!-- Page Heading -->
        <h1 class="h3 my-2 text-gray-800 font-weight-bold">Kategori Produk</h1>
        <a href="/kategori/create" class="btn btn-primary my-3">Tambah Kategori</a>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Daftar Kategori Tersedia</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 10%;">No</th>
                                <th style="width: 60%;">Nama Kategori</th>
                                <th style="width: 30%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($kategories as $kategori)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $kategori->nama }}</td>
                                    <td>
                                        <form action="/kategori/{{ $kategori->id }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <a href="/kategori/{{$kategori->id}}/edit" class="btn btn-warning">edit</a>
                                            <button type="submit" class="btn btn-danger">hapus</button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="3">Belum ada tugas tersedia</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
@endsection
