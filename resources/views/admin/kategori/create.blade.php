@extends('layouts.admin')

@section('content')
    <a class="btn btn-info mb-4" href="/kategori">Kembali</a>
    <div class="card shadow mb-4">
        <div class="card-body p-3">
                <div class="header text-dark">
                    <h2 class="font-weight-bold">Tambah Kategori</h2>
                    <hr />
                </div>
                <form action="/kategori" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama Kategori</label>
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Kategori" value="{{ old('kategori') }}">
                    </div>
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
    </div>
@endsection
