@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="mt-5 mb-3">
            <h2 class="text-center">Produk Tersedia</h2>
            <hr />
        </div>
        <div class="row justify-content-center mb-5">
            @foreach ($produks as $produk)
                <div class="col-sm-12 col-md-6 col-lg-3 mx-3 mb-3">
                    <div class="card" style="width: 18rem;">
                        <img src="{{ asset('gambar/' . $produk->gambar) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $produk->nama}}</h5>
                            <p class="card-text">{{ $produk->harga}}</p>
                            <a href="#" class="btn btn-primary">Beli Produk</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
