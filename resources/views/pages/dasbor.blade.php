@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="mt-5 mb-3">
            <h2 class="text-center">Daftar Transaksi</h2>
            <hr />
        </div>
        <div class="card-body mb-5">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width: 5%;">No</th>
                            <th style="width: 30%;">Nama barang</th>
                            <th style="width: 15%;">Jumlah Barang</th>
                            <th style="width: 25%;">Total Harga</th>
                            <th style="width: 25%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($transaksi as $ts)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $ts->produk->nama }}</td>
                                <td>{{ $ts->jumlah }}</td>
                                <td>{{ $ts->total }}</td>
                                <td>
                                    <form action="/transaksi/{{ $ts->id }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/transaksi/{{$ts->id}}" class="btn btn-info">detail</a>
                                        <a href="/transaksi/{{$ts->id}}/edit" class="btn btn-warning">edit</a>
                                        <button type="submit" class="btn btn-danger">hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="5">Belum ada transaksi</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
