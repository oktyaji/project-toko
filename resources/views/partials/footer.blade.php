<footer class="bg-dark text-center" id="tempaltemo_footer">
    <div class="w-100 bg-black py-3">
        <div class="container">
            <div class="row pt-2">
                <div class="col-12">
                    <p class="text-white">Anda Admin? <a href="/produk">Login Disini</a></p>
                    <p class="text-left text-light">
                        Copyright &copy; 2021 PKS Digital School
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
