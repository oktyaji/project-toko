<nav class="navbar navbar-expand-lg navbar-light shadow">
    <div class="container d-flex justify-content-between align-items-center">

        <a class="navbar-brand text-success logo h1 align-self-center" href="/">
            Serba
        </a>

        <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
            <div class="flex-fill">
                <ul class="nav navbar-nav d-flex justify-content-center mx-lg-auto">
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="/shop">Shop</a>
                    </li>
                </ul>
            </div>
            @auth
                <div class="navbar align-self-center d-flex">
                    <a href="/dasbor" class="btn btn-info mr-3 text-white">Dashboard</a>
                    <form action="/logout" method="POST">
                        @csrf
                        <button class="btn btn-danger" type="submit">Logout</button>
                    </form>
                </div>
            @else
                <a class="btn btn-success text-white font-weight-bold" href="/login">Login</a>
                <a class="btn btn-outline-success font-weight-bold" href="/register">Daftar</a>
            @endauth
        </div>
    </div>
</nav>
