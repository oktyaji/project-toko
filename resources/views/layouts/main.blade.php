<!DOCTYPE html>
<html lang="en">

<head>
    <title>Zay Shop eCommerce HTML CSS Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ asset('tamu/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('tamu/assets/css/templatemo.css')}}">
    <link rel="stylesheet" href="{{ asset('tamu/assets/css/custom.css')}}">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="{{ asset('tamu/assets/css/fontawesome.min.csss')}}">
<!--

TemplateMo 559 Zay Shop

https://templatemo.com/tm-559-zay-shop

-->
</head>

<body>
    @include('partials.navbar')

    @yield('content')

    @include('partials.footer')

    <!-- Start Script -->
    <script src="{{ asset('/tamu/assets/js/jquery-1.11.0.min.js') }}"></script>
    <script src="{{ asset('/tamu/assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ asset('/tamu/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/tamu/assets/js/templatemo.js') }}"></script>
    <script src="{{ asset('/tamu/assets/js/custom.js') }}"></script>
    <!-- End Script -->
</body>

</html>
